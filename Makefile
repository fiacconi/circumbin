# ============================================ #

OPT += -DWITHOMP
OPT += -DPHYSICALUNITS

# ============================================ #

CC=gcc
OPTIONS=-Wall -O2
EXEC=circumbin

# ============================================ #

GSLINC=-I/usr/include
GSLLIB=-L/usr/lib64

# ============================================ #
# ============================================ #
# ============================================ #

OPTIONS += $(OPT) $(GSLINC) -I.
ifeq (WITHOMP,$(findstring WITHOMP,$(OPT)))
OPTIONS += -fopenmp
endif

# ============================================ #

LIBS=$(GSLLIB) -lm -lgsl -lgslcblas

SOURCES = main.c io.c circumbin.c
OBJ = $(SOURCES:.c=.o)

all: allinc.h $(OBJ)
	$(CC) $(OPTIONS) -o $(EXEC) $(OBJ) $(LIBS)

main.o:
	$(CC) $(OPTIONS) -c main.c

io.o:
	$(CC) $(OPTIONS) -c io.c

circumbin.o:
	$(CC) $(OPTIONS) -c circumbin.c

clean:
	rm -f $(EXEC)
	rm -f $(OBJ)
	rm -f initcond.dat

# ============================================ #
