#include "allinc.h"

void usage() {
	fprintf(stdout, "\nUSAGE:\n...$ ./circumbin <parameter_file_name> <OPTIONAL: number of OpenMP threads>\n\n");
	exit(EXIT_SUCCESS);
}

void check_input_arguments(int argc, char **argv, CBD *cbd) {

	fprintf(stdout, ">> Checking input arguments.\n");

	if (argc <= 1) terminate("Parameter file not specified!\n");
	if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) usage();
	strcpy(cbd->parameter_file, argv[1]);
#ifdef WITHOMP
	if (argc <=2) terminate("Number of threads for OpenMP not specified!\n");
	cbd->NThreads = (int)atoi(argv[2]);
	omp_set_num_threads(cbd->NThreads);
#endif
	return;
}

void init_model(CBD *cbd) {

	FILE *f;
	char buf[MAXCHAR], *param, *value;
	int i, tmp = 0;

	fprintf(stdout, ">> Reading parameter file and initialising the model.\n");

	f = fopen(cbd->parameter_file, "r");
	if (f == NULL) terminate("Cannot open parameter file!\n");

	cbd->alpha_sd = 0.0;
	cbd->bbh_inc_angle = 0.0;

#ifdef PHYSICALUNITS
	cbd->bh_mass_binary = cbd->bh_init_sep = 0.0;
#endif

	while (!feof(f)) {
		fgets(buf, MAXCHAR, f);

		if (buf[0] == '#' || buf[0] == '%' || buf[0] == '\n') continue;

		param = strtok(buf, " \t");
		value = strtok(NULL, " \t\n");

		if (!strcmp(param, "RandomSeed")) cbd->random_seed = (unsigned long int)atoi(value);
		else if (!strcmp(param, "BoxSize")) cbd->BoxSize = strtod(value, NULL);
		else if (!strcmp(param, "NumGas")) cbd->ngas = (int)atoi(value);
		else if (!strcmp(param, "BhMassRatio")) cbd->bh_mass_ratio = strtod(value, NULL);
		else if (!strcmp(param, "BhDiscMassRatio")) cbd->bhdisc_mass_ratio = strtod(value, NULL);
		else if (!strcmp(param, "Eccentricity")) cbd->ecc = strtod(value, NULL);
		else if (!strcmp(param, "BinaryIncAngle")) cbd->bbh_inc_angle = strtod(value, NULL);
#ifdef PHYSICALUNITS
		else if (!strcmp(param, "BhMassBinary")) cbd->bh_mass_binary = strtod(value, NULL);
		else if (!strcmp(param, "BhInitA")) cbd->bh_init_sep = strtod(value, NULL);
#endif
		else if (!strcmp(param, "DiscAlpha")) cbd->alpha_sd = strtod(value, NULL);
		else if (!strcmp(param, "DiscVerticalStructure")) tmp = atoi(value);
		else fprintf(stderr, "Parameter '%s' not understood, skipping.\n", param);
	}

	fclose(f);

	if ((cbd->bh_mass_ratio < 0.0) || (cbd->bh_mass_ratio > 1.0)) terminate("The BH mass ratio must be within 0 and 1!\n");
	if ((cbd->bhdisc_mass_ratio < 0.0) || (cbd->bhdisc_mass_ratio > 1.0)) terminate("The BH-disc mass ratio must be within 0 and 1!\n");
	if ((cbd->ecc < 0.0) || (cbd->ecc > 1.0)) terminate("The eccentricity must be within 0 and 1!\n");
	if ((cbd->alpha_sd < 1.0) || (cbd->alpha_sd > 3.0)) terminate("The disc SD slope must be 1 <= |alpha| <= 3!\n");
	if ((tmp != 0) && (tmp != 1)) terminate("'DiscVerticalStructure' can be 0 = UNIFORM ASPECT RATIO, or 1 = UNIFORM TEMPERATURE\n");
	if ((cbd->ecc < 0.0) || (cbd->ecc > 1.0)) terminate("The eccentricity must be within 0 and 1!\n");

	cbd->bbh_inc_angle = fmod(cbd->bbh_inc_angle, 360.) * PI / 180.0;

	/* Fix the vertical structure variables */
	cbd->constant_aspect_ratio = QTOOMRE * cbd->bhdisc_mass_ratio / 3.2;
	if (tmp == 0) cbd->constant_cs = 0.0;
	else {
		cbd->constant_cs = cbd->constant_aspect_ratio / sqrt(RIN);
#ifdef PHYSICALUNITS
		printf(">> Constant sound speed = %.3lf km/s\n", cbd->constant_cs * sqrt(cbd->bh_mass_binary / UNITS_MSOL / cbd->bh_init_sep));
#else
		printf(">> Constant sound speed = %.3lf CU\n", cbd->constant_cs);
#endif
	}

	cbd->mbh_1 = 1.0 / (1. + cbd->bh_mass_ratio);
	cbd->mbh_2 = cbd->bh_mass_ratio / (1. + cbd->bh_mass_ratio);
#ifdef PHYSICALUNITS
	if (cbd->bh_mass_binary == 0.0) terminate("The mass of the binary must be specified in Msol!\n");
	if (cbd->bh_init_sep == 0.0) terminate("The initial BH semi-major axis must be specified in pc!\n");
	cbd->bh_mass_binary /= UNITS_MSOL;
	cbd->bh_init_sep /= UNITS_PC;
	cbd->mbh_1 *= cbd->bh_mass_binary;
	cbd->mbh_2 *= cbd->bh_mass_binary;
	cbd->BoxSize *= cbd->bh_init_sep;
#endif
	cbd->p = (PART *)malloc(sizeof(PART) * (cbd->ngas + 2));

#ifdef WITHOMP
	cbd->rng = (gsl_rng **)malloc(cbd->NThreads * sizeof(gsl_rng *));

	#pragma omp parallel default(shared) private(i)
	{
		int nid = omp_get_thread_num(), npart = cbd->ngas+2;
		#pragma omp critical
		{
			cbd->rng[nid] = gsl_rng_alloc(gsl_rng_mt19937);
		}
		if (cbd->rng[nid] == NULL) terminate("Not enough memory to create the random number generators!\n");
		gsl_rng_set(cbd->rng[nid], cbd->random_seed+nid);

		#pragma omp for schedule(static)
		for (i=0; i<npart; ++i) {
			cbd->p[i].pos[0] = cbd->p[i].pos[1] = cbd->p[i].pos[2] = 0.0;
			cbd->p[i].vel[0] = cbd->p[i].vel[1] = cbd->p[i].vel[2] = 0.0;
		}
	}
#else
	cbd->rng = gsl_rng_alloc(gsl_rng_mt19937);
	if (cbd->rng == NULL) terminate("Not enough memory to create the random generator!\n");
	gsl_rng_set(cbd->rng, cbd->random_seed);
#endif

	return;
}

void free_mem(CBD *cbd) {

	fprintf(stdout, ">> Freeing the memory.\n");

#ifdef WITHOMP
	#pragma omp parallel
	{
		int nid = omp_get_thread_num();
		#pragma omp critical
		{
			gsl_rng_free(cbd->rng[nid]);
		}
	}
	free(cbd->rng);
#else
	if (cbd->rng != NULL) gsl_rng_free(cbd->rng);
#endif
	if (cbd->p != NULL) {
		free(cbd->p);
	}
	return;
}

void write_ic(CBD *cbd) {

	int sizeBox, i, j, npart=0;
	FILE *initcond;
	HDR h;
	unsigned int ii;
	float u, tmp[3];

	for (i=0; i<6; ++i) {
		h.npart[i] = 0;
		h.mass[i] = 0;
		h.npartTotal[i] = 0;
		h.npartTotalHighWord[i] = 0;
	}

	h.npart[0] = h.npartTotal[0] = cbd->ngas;
	h.npart[5] = h.npartTotal[5] = 2;

	h.mass[0] = (cbd->mbh_2 + cbd->mbh_1) * cbd->bhdisc_mass_ratio / cbd->ngas;

	h.time = 0.0;
	h.redshift = 0.0;
	h.flag_sfr = 0;
	h.flag_feedback = 0;
	h.flag_cooling = 0;
	h.num_files = 1;
	h.BoxSize = cbd->BoxSize;
	h.Omega0 = 0.0;
	h.OmegaLambda = 0.0;
	h.HubbleParam = 0.0;
	h.flag_stellarage = 0;
	h.flag_metals = 0;
	h.flag_entropy_instead_u = 0;
	h.flag_doubleprecision = 0;
	h.flag_lpt_ics = 0;
	h.lpt_scalingfactor = 0.0;
	h.flag_tracer_field = 0;
	h.composition_vector_length = 0;

	for (i=0;i<40;++i) h.fill[i] = 0;

	fprintf(stdout, ">> Writing initial conditions in file 'initcond.dat'\n");

	/* Open file and check */
	initcond = fopen("initcond.dat","wb");
	if (initcond == NULL) terminate("Cannot open file 'initcond.dat'!\n");

	/* Write the header */
	sizeBox = sizeof(HDR);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	fwrite(&h, sizeof(HDR), 1, initcond);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

	npart = cbd->ngas;

	/* Positions */
	sizeBox = 3 * (npart + 2) * sizeof(float);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	for (i=0; i<(npart+2); ++i) {
		for (j=0; j<3; ++j) tmp[j] = (float)(cbd->p[i].pos[j] + 0.5 * cbd->BoxSize);
		fwrite(tmp, sizeof(float), 3, initcond);
	}
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

	/* Velocities */
	sizeBox = 3 * (npart + 2) * sizeof(float);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	for (i=0; i<(npart+2); ++i) {
		for (j=0; j<3; ++j) tmp[j] = (float)(cbd->p[i].vel[j]);
		fwrite(tmp, sizeof(float), 3, initcond);
	}
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

	/* IDs */
	sizeBox = (npart + 2) * sizeof(unsigned int);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	for (i=1; i<=(npart+2); ++i) { 
		ii = (unsigned int)i;
		fwrite(&ii, sizeof(unsigned int), 1, initcond);
	}
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

	/* BH Masses */
	sizeBox = 2 * sizeof(float);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	tmp[0] = (float)(cbd->mbh_1);
	tmp[1] = (float)(cbd->mbh_2);
	fwrite(tmp, sizeof(float), 2, initcond);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

	/* Internal energies */
	sizeBox = npart * sizeof(float);
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);
	for(i=0; i<npart; ++i) {
		u = (float)(cbd->p[i].u);
		fwrite(&u, sizeof(float), 1, initcond);
	}
	fwrite(&sizeBox, sizeof(sizeBox), 1, initcond);

	fclose(initcond);
	return;
}

