#include "allinc.h"

/************************************************************/

void make_pos(CBD *cbd);
void make_vel(CBD *cbd);
double zbrent(double (*func)(double), double x1, double x2, double tol);

/************************************************************/

void make_circumbin(CBD *cbd) {
	make_pos(cbd);
	make_vel(cbd);
	return;
}

/************************************************************/

double azimuthal_angle(gsl_rng *rng) {
	return 2.0 * PI * gsl_rng_uniform(rng);
}

double radial_coord(gsl_rng *rng, double a) {

	double y = gsl_rng_uniform(rng);

	if (a == 2.0) {
		return RIN * pow(ROUT / RIN, y);
	}
	else {
		return RIN * pow(1. + y * (pow(ROUT/RIN, 2. - a) - 1.), 1./(2. - a));
	}
}

double vertical_coord(gsl_rng *rng, double h, double r) {
	return gsl_ran_gaussian(rng, h*r);
}

void make_pos(CBD *cbd) {

	int i;

	fprintf(stdout, ">> Initialising particle positions.\n");

#ifdef WITHOMP
	#pragma omp parallel default(shared) private(i)
	{
		int nid = omp_get_thread_num();
		gsl_rng *local_rng;
		local_rng = cbd->rng[nid];
#else
		gsl_rng *local_rng = cbd->rng[nid];
#endif
		PART *pp = cbd->p;
		int npart = cbd->ngas;
		double aspect_ratio = cbd->constant_aspect_ratio;
		double sound_speed = cbd->constant_cs;
		double alpha = cbd->alpha_sd;
		double r, phi, z;
#ifdef PHYSICALUNITS
		double length_scale = cbd->bh_init_sep;
#else
		double length_scale = 1;
#endif
#ifdef WITHOMP
		#pragma omp for schedule(static)
		for (i=0; i<npart; ++i)
#else
		for (i=0; i<npart; ++i)
#endif
		{
			r = radial_coord(local_rng, alpha) * length_scale;

			if (sound_speed > 0) z = vertical_coord(local_rng, aspect_ratio * sqrt(r/RIN), r) * length_scale;
			else z = vertical_coord(local_rng, aspect_ratio, r) * length_scale;

			phi = azimuthal_angle(local_rng);

			pp[i].pos[0] = r * cos(phi);
			pp[i].pos[1] = r * sin(phi);
			pp[i].pos[2] = z;
		}
#ifdef WITHOMP
	}
#endif

	/* FIRST BH1, MORE MASSIVE ONE */
	cbd->p[cbd->ngas].pos[0] = cbd->mbh_2 * A0 * (1. + cbd->ecc) / (cbd->mbh_2 + cbd->mbh_1);
	cbd->p[cbd->ngas].pos[1] = 0;
	cbd->p[cbd->ngas].pos[2] = 0;

	/* THEN BH2, LESS MASSIVE ONE */
	cbd->p[cbd->ngas+1].pos[0] = -cbd->mbh_1 * A0 * (1. + cbd->ecc) / (cbd->mbh_2 + cbd->mbh_1);
	cbd->p[cbd->ngas+1].pos[1] = 0;
	cbd->p[cbd->ngas+1].pos[2] = 0;

#ifdef PHYSICALUNITS
	cbd->p[cbd->ngas].pos[0] *= cbd->bh_init_sep;
	cbd->p[cbd->ngas+1].pos[0] *= cbd->bh_init_sep;
#endif

	/* ROTATE THE BINARY ALONG Y AXIS */
	cbd->p[cbd->ngas].pos[2] = cbd->p[cbd->ngas].pos[0] * sin(cbd->bbh_inc_angle);
	cbd->p[cbd->ngas].pos[0] = cbd->p[cbd->ngas].pos[0] * cos(cbd->bbh_inc_angle);

	cbd->p[cbd->ngas+1].pos[2] = cbd->p[cbd->ngas+1].pos[0] * sin(cbd->bbh_inc_angle);
	cbd->p[cbd->ngas+1].pos[0] = cbd->p[cbd->ngas+1].pos[0] * cos(cbd->bbh_inc_angle);

	return;
}

/************************************************************/

void make_vel(CBD *cbd) {

	int i;

	fprintf(stdout, ">> Initialising particle velocities.\n");

#ifdef WITHOMP
	#pragma omp parallel default(shared) private(i)
	{
		int nid = omp_get_thread_num();
		gsl_rng *local_rng;
		local_rng = cbd->rng[nid];
#else
		gsl_rng *local_rng = cbd->rng[nid];
#endif
		PART *pp = cbd->p;
		PART *pp1 = cbd->p + cbd->ngas;
		PART *pp2 = cbd->p + cbd->ngas + 1;
		int npart = cbd->ngas;
		double aspect_ratio = cbd->constant_aspect_ratio;
		double sound_speed = cbd->constant_cs;
		double alpha = cbd->alpha_sd;
		double r, r1, r2, m1 = cbd->mbh_1 / (cbd->mbh_1 + cbd->mbh_2), m2 = cbd->mbh_2 / (cbd->mbh_1 + cbd->mbh_2);
		double vphi;
#ifdef PHYSICALUNITS
		double vnorm = sqrt(cbd->bh_mass_binary / cbd->bh_init_sep);
		double length_scale = cbd->bh_init_sep;
#else
		double length_scale = 1;
#endif
#ifdef WITHOMP
		#pragma omp for schedule(static)
		for (i=0; i<npart; ++i)
#else
		for (i=0; i<npart; ++i)
#endif
		{
			r = sqrt(pp[i].pos[0]*pp[i].pos[0] + pp[i].pos[1]*pp[i].pos[1]) / length_scale;
			r1 = sqrt(pow(pp[i].pos[0] - (*pp1).pos[0], 2) + pow(pp[i].pos[1] - (*pp1).pos[1], 2)) / length_scale;
			r2 = sqrt(pow(pp[i].pos[0] - (*pp2).pos[0], 2) + pow(pp[i].pos[1] - (*pp2).pos[1], 2)) / length_scale;

			if (sound_speed > 0.0) {
				vphi = sqrt((1 + 3. / 4. * (m1 * m2) / r / r) / r  - sound_speed * sound_speed * alpha);
				pp[i].u = sound_speed * sound_speed / (GAMMA - 1);
			}
			else {
				sound_speed = aspect_ratio * sqrt(m1/r1 + m2/r2);
				vphi = sqrt((1 + 3. / 4. * (m1 * m2) / r / r) / r  - sound_speed * sound_speed * (alpha + 1));
				pp[i].u = sound_speed * sound_speed / (GAMMA - 1);
				sound_speed = 0.0;
			}

			pp[i].vel[0] = -vphi * pp[i].pos[1] / r / length_scale;
			pp[i].vel[1] = vphi * pp[i].pos[0] / r / length_scale;
			pp[i].vel[2] = 0.0;

#ifdef PHYSICALUNITS
			pp[i].vel[0] *= vnorm;
			pp[i].vel[1] *= vnorm;
			pp[i].vel[2] *= vnorm;
			pp[i].u *= (vnorm * vnorm);
#endif
		}
#ifdef WITHOMP
	}
#endif

	/* FIRST BH1, MORE MASSIVE ONE */
	cbd->p[cbd->ngas].vel[0] = 0;
	cbd->p[cbd->ngas].vel[1] = cbd->mbh_2 * sqrt((1. - cbd->ecc) / (1. + cbd->ecc) / A0) / (cbd->mbh_2 + cbd->mbh_1);
	cbd->p[cbd->ngas].vel[2] = 0;

	/* THEN BH2, LESS MASSIVE ONE */
	cbd->p[cbd->ngas+1].vel[0] = 0;
	cbd->p[cbd->ngas+1].vel[1] = -cbd->mbh_1 * sqrt((1. - cbd->ecc) / (1. + cbd->ecc) / A0) / (cbd->mbh_2 + cbd->mbh_1);
	cbd->p[cbd->ngas+1].vel[2] = 0;

#ifdef PHYSICALUNITS
	cbd->p[cbd->ngas].vel[1] *= sqrt(cbd->bh_mass_binary / cbd->bh_init_sep);
	cbd->p[cbd->ngas+1].vel[1] *= sqrt(cbd->bh_mass_binary  / cbd->bh_init_sep);
#endif

	return;
}

/************************************************************/

double zbrent(double (*func)(double), double x1, double x2, double tol) {
#define ITMAX 100
#define EPS 3.0e-8
	int iter;
	double a=x1,b=x2,c=x2,d,e,min1,min2;
	double fa=(*func)(a),fb=(*func)(b),fc,p,q,r,s,tol1,xm;

	if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)) terminate("Root must be bracketed in zbrent!\n");

	fc=fb;
	for (iter=1; iter<=ITMAX; iter++) {
		if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
			c=a;
			fc=fa;
			e=d=b-a;
		}
		if (fabs(fc) < fabs(fb)) {
			a=b;
			b=c;
			c=a;
			fa=fb;
			fb=fc;
			fc=fa;
		}
		tol1=2.0*EPS*fabs(b)+0.5*tol;
		xm=0.5*(c-b);
		if (fabs(xm) <= tol1 || fb == 0.0) return b;
		if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
			s=fb/fa;
			if (a == c) {
				p=2.0*xm*s;
				q=1.0-s;
			} else {
				q=fa/fc;
				r=fb/fc;
				p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				q=(q-1.0)*(r-1.0)*(s-1.0);
			}
			if (p > 0.0) q = -q;
			p=fabs(p);
			min1=3.0*xm*q-fabs(tol1*q);
			min2=fabs(e*q);
			if (2.0*p < (min1 < min2 ? min1 : min2)) {
				e=d;
				d=p/q;
			} else {
				d=xm;
				e=d;
			}
		} else {
			d=xm;
			e=d;
		}
		a=b;
		fa=fb;
		if (fabs(d) > tol1) {
			b += d;
		} else {
			b += SIGN(tol1,xm);
		}
		fb=(*func)(b);
	}
	terminate("Maximum number of iterations exceeded in zbrent!\n");
	return 0.0;
}
