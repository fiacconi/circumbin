#ifndef __ALLINC_H__
#define __ALLINC_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"
#include "gsl/gsl_sf_gamma.h"

#ifdef WITHOMP
#include <omp.h>
#endif

#define MAXCHAR 240
#define PI 3.141592654
#define GAMMA (5./3.)
#define G 1.0

#ifdef PHYSICALUNITS
#define UNITS_MSOL 2.3262e2
#define UNITS_PC 1.0
#define MSOL_CGS 1.9891e33
#define PC_CGS 3.08567e18
#define PROTON_MASS 1.6726219e-24
#define BOLTZMANN_CONST 1.38064852e-16
#define MMW 0.59
#endif

#define terminate(x) {fprintf(stderr, "ERROR!\n"); fprintf(stderr, (x)); exit(EXIT_FAILURE);}

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

#define A0 1.0
#define RIN 2.0
#define ROUT 7.0
#define QTOOMRE 1.5

typedef struct io_gadget_header {
	int npart[6];
	double mass[6];
	double time;
	double redshift;
	int flag_sfr;
	int flag_feedback;
	unsigned int npartTotal[6];
	int flag_cooling;
	int num_files;
	double BoxSize;
	double Omega0;
	double OmegaLambda;
	double HubbleParam;
	int flag_stellarage;
	int flag_metals;
	unsigned int npartTotalHighWord[6];
	int flag_entropy_instead_u;
	int flag_doubleprecision;
	int flag_lpt_ics;
	float lpt_scalingfactor;
	int flag_tracer_field;
	int composition_vector_length;
	char fill[40];
} HDR;

typedef struct {
	double pos[3];
	double vel[3];
	double u;
} PART;

typedef struct {

#ifdef WITHOMP
	int NThreads;
	gsl_rng **rng;
#else
	gsl_rng *rng;
#endif
	char parameter_file[MAXCHAR];

	unsigned long int random_seed;

	double BoxSize;

	int ngas;
	double bh_mass_ratio;
	double bhdisc_mass_ratio;
	double ecc;
	double bbh_inc_angle;

	double alpha_sd;
	double constant_aspect_ratio;
	double constant_cs;

#ifdef PHYSICALUNITS
	double bh_mass_binary;
	double bh_init_sep;
#endif

	double mbh_1;
	double mbh_2;

	PART *p;
} CBD;

void check_input_arguments(int argc, char **argv, CBD *cbd);
void init_model(CBD *cbd);
void make_circumbin(CBD *cbd);
void write_ic(CBD *cbd);
void free_mem(CBD *cbd);

#endif

