*** CIRCUMBINARY DISC ***

AUTHOR: Davide Fiacconi, Institute of Astronomy, University of Cambridge

EMAIL: fiacconi AT ast.cam.ac.uk

VERSION: 1.0

This code builds the initial conditions (ICs) for a Keplerian supermassive black hole
(SMBH) binary surrounded by a gaseous circumbinary disc. The disc extends from 2 to
7 times the semi-major axis of the binary and follow a power-law surface density.
The thickness of the disc is set either to satisfiy a uniform aspect ratio or a 
uniform temperature across the disc. The gas is in nearly Keplerian velocity, corrected
at first order for the tidal perturbation of the binary and the thermal pressure.
The code is written in C and runs on multiple threads through OpenMP.

** DEPENDENCIES: **

The code uses several routines from the Gnu Scientific Libraries (GSL). They are
available at the GSL website, http://www.gnu.org/software/gsl/. Before compiling,
GSL must be compiled and installed in the system.

** HOW TO BUILD: **

The directory contains the source code with a Makefile for automatic compilation. 
The user only requires a C compiler (e.g. gcc) to build the code, once the GSL are 
installed (see ** DEPENDENCES ** above).

The Makefile contains some compiling-time options (also described in the Makefile):

- -DWITHOMP        : this option enables the OpenMP parallelization.
- -DPHYSICALUNITS  : this option produces output in physical units instead of
                     Mbin = semi-maj. axis = G = 1.

Before compiling the code, enable/disable the desired compiling-time options and
set the right paths for the GSL headers and libraries in the variables GSLINC and
GSLLIB, respectively. Then, on UNIX systems, just type

...$ make

and the code should compile. If the compilation is successful, the executable file
"circumbin" should be crated. Typing "make clean" would erase all the object files, 
the executable and any output file produced by the code.

** HOW TO RUN: **

To run the code, use the following syntax

...$ ./circumbin <parameter_file> [OPTIONS]

The code requires a parameter file to run. An example is shown in the "input" file
included in the package (references to the values of the parameters can be found by
checking the file "io.c", function "init_model").

The code can be run to produce the ICs as

...$ ./circumbin parameter_file_name <integer_number_of_OpenMP_threads>

where the last parameter specifies the number of threads used by the OpenMP. This
parameter is required ONLY IF the code was compiled with the -DWITHOMP option,
otherwise is ignored. The code will produce the file "initcond.dat" in Arepo/GADGET2
format. If the option -DPHYSICALUNITS is enabled, the output will have the following
units:

- [LENGTH] = 1 pc
- [VELOCITY] = 1 km/s
- [MASS] = 2.3262e2 Msol

which implies [TIME] = 0.978 Myr and G = 1 in code units.
