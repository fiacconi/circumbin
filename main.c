#include "allinc.h"

int main(int argc, char **argv) {

	CBD MyModel;

	check_input_arguments(argc, argv, &MyModel);

	init_model(&MyModel);

	make_circumbin(&MyModel);

	write_ic(&MyModel);

	free_mem(&MyModel);

	exit(EXIT_SUCCESS);
}

